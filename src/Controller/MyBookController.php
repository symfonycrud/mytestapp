<?php

namespace App\Controller;

use App\Entity\MyBook;
use App\Form\MyBookType;
use App\Repository\MyBookRepository;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



/**
 * @Route("/book")
 */
class MyBookController extends AbstractController
{
    /**
     * @Route("/", name="my_book_index", methods={"GET"})
     */
    public function index(MyBookRepository $myBookRepository): Response
    {
        return $this->render('my_book/index.html.twig', [
            'my_books' => $myBookRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="my_book_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $myBook = new MyBook();
        $form = $this->createForm(MyBookType::class, $myBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($myBook);
            $entityManager->flush();

            return $this->redirectToRoute('my_book_index');
        }

        return $this->render('my_book/new.html.twig', [
            'my_book' => $myBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="my_book_show", methods={"GET"})
     * @SWG\Response(
     *     description="Получить запись по id",
     *     response="200"
     * )
     *
     */
    public function show(MyBook $myBook): Response
    {
        return $this->render('my_book/show.html.twig', [
            'my_book' => $myBook,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="my_book_edit", methods={"GET","POST"})
     * @
     */
    public function edit(Request $request, MyBook $myBook): Response
    {
        $form = $this->createForm(MyBookType::class, $myBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('my_book_index', [
                'id' => $myBook->getId(),
            ]);
        }

        return $this->render('my_book/edit.html.twig', [
            'my_book' => $myBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="my_book_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MyBook $myBook): Response
    {
        if ($this->isCsrfTokenValid('delete'.$myBook->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($myBook);
            $entityManager->flush();
        }

        return $this->redirectToRoute('my_book_index');
    }
}
